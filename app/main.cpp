#include "./caaarController.hpp"

int main() {
    CaaarController controller;

    if (auto detectedPort = controller.autoDetect()) {
        std::cout << "CaaarController detected on port " << *detectedPort << "!" << std::endl;

        // Example usage of the CaaarController class
        controller.setLEDMode(1);
        controller.setLEDColor(0, 255, 255, 255);
        controller.setServoDutyCycle(0, 128);

    } else {
        std::cout << "CaaarController not detected." << std::endl;
    }

    return 0;
}